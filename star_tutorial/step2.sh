#!/bin/bash
STAR \
    --runThreadN 4 \
    --genomeDir ${GENOME_INDEX} \
    --readFilesIn ${R1} \
    --outFileNamePrefix "${OUTPUT}/"