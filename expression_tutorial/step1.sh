#!/bin/bash
# a) Sort SAM file on genomic position.
samtools sort \
    --threads 2 \
    --output-fmt BAM \
    -o ${BAM} \
    ${SAM}
# b) Create index of sorted BAM file.
samtools index \
    -@ 2 \
    ${BAM} \
    ${BAI}