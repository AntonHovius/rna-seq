#!/bin/bash
# Compute expression
htseq-count \
    --stranded=reverse \
    --order pos \
    --format bam \
    --idattr gene_id \
    --additional-attr=gene_name \
    --type gene \
    -n 2 \
    ${BAM} \
    ${GTF} \
    -c ${COUNTS}