#!/bin/bash
dsub \
    --provider google-cls-v2 \
    --project <my-project> \
    --location europe-west4 \
    --zones europe-west4-a \
    --preemptible \
    --min-ram 8 \
    --min-cores 2 \
    --logging "${BUCKET}/logging/step2/" \
    --input GTF=${REFDATA}/gencode.v36.chr_patch_hapl_scaff.annotation.gff3 \
    --image registry.gitlab.com/hylkedonker/rna-seq \
    --tasks step2.tsv \
    --script step2.sh