#!/bin/bash
echo -e "${GTF_SAMPLE1}\n${GTF_SAMPLE2}" > ./assemblies.txt
python2 /usr/bin/cuffmerge \
        --ref-gtf ${GTF_FILE} \
        --ref-sequence ${FASTA_FILE} \
        -o ${OUTPUT_FOLDER} \
        --num-threads 2 \
        ./assemblies.txt