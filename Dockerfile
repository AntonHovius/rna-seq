FROM ubuntu:21.04

RUN apt update && apt install \
    --no-install-recommends \
    --quiet \
    --assume-yes \
    samtools=1.11-1 \
    rna-star=2.7.8a+dfsg-2 \
    python3-htseq=0.13.5-1

RUN mkdir -p /pipeline/output/
RUN mkdir -p /pipeline/genome_index/
WORKDIR /pipeline/

